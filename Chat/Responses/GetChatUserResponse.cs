﻿using Chat.Models;

namespace Chat.Responses;

public class GetChatUserResponse
{
    public ChatUserModel User { get; set; }
}