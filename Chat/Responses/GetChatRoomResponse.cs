﻿using Chat.Models;

namespace Chat.Responses;

public class GetChatRoomResponse
{
    public ChatRoomModel ChatRoomModel { get; set; }
}