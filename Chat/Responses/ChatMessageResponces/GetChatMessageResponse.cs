﻿using Chat.Models;

namespace Chat.Responses;

public class GetChatMessageResponse
{
    public ChatMessageModel ChatMessageModel { get; set; }
}