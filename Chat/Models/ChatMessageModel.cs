﻿namespace Chat.Models;

public class ChatMessageModel
{
    public string Text { get; set; }
    public int AuthorId { get; set; }
    public int ChatId { get; set; }
}