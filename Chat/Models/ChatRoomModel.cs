namespace Chat.Models;

public class ChatRoomModel
{
    public int Id { get; set; }
    public ChatUserModel[] Users { get; set; }
    
    public ChatMessageModel[] Messages { get; set; }
}