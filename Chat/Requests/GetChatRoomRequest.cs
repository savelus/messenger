﻿using Chat.Responses;
using MediatR;

namespace Chat.Requests;

public class GetChatRoomRequest : IRequest<GetChatRoomResponse>
{
    public int Id { get; set; }
    public string Title { get; set; }
    public int[] MemberIds{ get; set; }

}