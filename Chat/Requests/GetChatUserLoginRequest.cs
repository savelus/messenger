﻿using Chat.Responses;
using MediatR;

namespace Chat.Requests;

public class GetChatUserLoginRequest : IRequest<GetChatUserResponse>
{
    public string Login { get; set; }
}