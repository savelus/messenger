﻿using Chat.Models;
using Chat.Responses;
using MediatR;

namespace Chat.Requests;

public class CreateUserRequest : IRequest<CreateUserResponse>
{
    public ChatUserModel User { get; set; }
}