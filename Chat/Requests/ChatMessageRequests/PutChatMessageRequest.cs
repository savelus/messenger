﻿using Chat.Responses;
using MediatR;

namespace Chat.Requests.ChatMessageRequests;

public class PutChatMessageRequest : IRequest<GetChatMessageResponse>
{
    public int ChatId { get; set; }
    public int MessageId { get; set; }
    public string Text { get; set; }
}