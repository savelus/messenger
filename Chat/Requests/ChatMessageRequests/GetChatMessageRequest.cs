﻿using Chat.Responses;
using MediatR;

namespace Chat.Requests.ChatMessageRequests;

public class GetChatMessageRequest : IRequest<GetChatMessageResponse>
{
    public int Id { get; set; }
    public int ChatId { get; set; }
}