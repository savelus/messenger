﻿using Chat.Responses;
using MediatR;

namespace Chat.Requests.ChatMessageRequests;

public class PostChatMessageRequest : IRequest<GetChatMessageResponse>
{
    public int Id { get; set; }
    public int ChatId { get; set; }
    public int AuthorId { get; set; }
    public string Text { get; set; }
}