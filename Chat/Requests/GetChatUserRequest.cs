﻿using Chat.Responses;
using MediatR;

namespace Chat.Requests;

public class GetChatUserRequest : IRequest<GetChatUserResponse>
{
    public string Name { get; set; }
    public string Login { get; set; }
}