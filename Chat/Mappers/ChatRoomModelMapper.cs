﻿using Chat.Data.Entities;
using Chat.Models;

namespace Chat.Mappers;

public class ChatRoomModelMapper
{
    private readonly ChatUserModelMapper _userModelMapper;
    private readonly ChatMessageModelMapper _messageModelMapper;

    public ChatRoomModelMapper(
        ChatUserModelMapper userModelMapper,
        ChatMessageModelMapper messageModelMapper)
    {
        _userModelMapper = userModelMapper;
        _messageModelMapper = messageModelMapper;
    }
    public ChatRoomModel Map(ChatRoom chatRoom)
    {
        return new()
        {
            Id = chatRoom.Id,
            Users = chatRoom.Users.Select(_userModelMapper.Map).ToArray(),
            Messages = chatRoom.Messages.Select(_messageModelMapper.Map).ToArray()
        };
    }
}