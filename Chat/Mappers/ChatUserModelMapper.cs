﻿using Chat.Data.Entities;
using Chat.Models;

namespace Chat.Mappers;

public class ChatUserModelMapper 
{
    public ChatUserModel Map(ChatUser user)
    {
        return new()
        {
            Id = user.Id,
            Login = user.Login,
            Name = user.Name,
            Password = user.Password,
            Role = (Role)user.Role
        };
    }
}