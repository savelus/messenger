﻿using Chat.Data.Entities;
using Chat.Models;

namespace Chat.Mappers;

public class ChatMessageModelMapper
{
    public ChatMessageModel Map(ChatMessage message)
    {
        return new()
        {
            AuthorId = message.AuthorId,
            ChatId = message.ChatRoomId,
            Text = message.Text
        };
    }
}