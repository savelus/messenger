﻿using Chat.Data;
using Chat.Data.Entities;
using Chat.Mappers;
using Chat.Requests.ChatMessageRequests;
using Chat.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Chat.Handlers.ChatMessageHandlers;

public class PostChatMessageHandler: IRequestHandler<PostChatMessageRequest, GetChatMessageResponse>
{
    private readonly ChatContext _ctx;
    private readonly ChatMessageModelMapper _modelMapper;

    public PostChatMessageHandler(ChatContext ctx, ChatMessageModelMapper modelMapper)
    {
        _ctx = ctx;
        _modelMapper = modelMapper;
    }
    public async Task<GetChatMessageResponse> Handle(PostChatMessageRequest request, CancellationToken cancellationToken)
    {
        var chatUser = await _ctx.ChatUsers.
            FirstOrDefaultAsync(x => x.Id == request.AuthorId, cancellationToken);
        var chatRoom = await _ctx.ChatRooms.
            FirstOrDefaultAsync(x => x.Id == request.ChatId, cancellationToken);
        ChatMessage message = new ChatMessage()
        {
            Text = request.Text,
            Author = chatUser,
            AuthorId = request.AuthorId,
            ChatRoom = chatRoom,
            ChatRoomId = request.ChatId
        };
        _ctx.ChatMessages.Add(message);
        chatRoom.Messages.Add(message);
        chatUser.Messages.Add(message);
        await _ctx.SaveChangesAsync(cancellationToken);
        return new GetChatMessageResponse()
        {
            ChatMessageModel = _modelMapper.Map(message)
        };
    }
}