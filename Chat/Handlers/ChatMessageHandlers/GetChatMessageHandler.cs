﻿using Chat.Data;
using Chat.Mappers;
using Chat.Requests.ChatMessageRequests;
using Chat.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Chat.Handlers.ChatMessageHandlers;

public class GetChatMessageHandler: IRequestHandler<GetChatMessageRequest, GetChatMessageResponse>
{
    private readonly ChatContext _ctx;
    private readonly ChatMessageModelMapper _modelMapper;

    public GetChatMessageHandler(ChatContext ctx, ChatMessageModelMapper modelMapper)
    {
        _ctx = ctx;
        _modelMapper = modelMapper;
    }

    public async Task<GetChatMessageResponse> Handle(GetChatMessageRequest messageRequest, CancellationToken cancellationToken)
    {
        var message = await _ctx.ChatMessages
            .FirstOrDefaultAsync(x => x.Id == messageRequest.Id && x.ChatRoomId == messageRequest.ChatId, cancellationToken);
        if (message is null)
            return null;
        return new GetChatMessageResponse()
        {
            ChatMessageModel = _modelMapper.Map(message)
        };
    }
}