﻿using Chat.Data;
using Chat.Mappers;
using Chat.Requests.ChatMessageRequests;
using Chat.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Chat.Handlers.ChatMessageHandlers;

public class PutChatMessageHandler: IRequestHandler<PutChatMessageRequest, GetChatMessageResponse>
{
    private readonly ChatContext _ctx;
    private readonly ChatMessageModelMapper _modelMapper;

    public PutChatMessageHandler(ChatContext ctx, ChatMessageModelMapper modelMapper)
    {
        _ctx = ctx;
        _modelMapper = modelMapper;
    }
    public async Task<GetChatMessageResponse> Handle(PutChatMessageRequest request, CancellationToken cancellationToken)
    {
        var existingMessage = await _ctx.ChatMessages
            .FirstOrDefaultAsync(x => x.Id == request.MessageId, cancellationToken);
        existingMessage.Text = request.Text;
        await _ctx.SaveChangesAsync(cancellationToken);
        return new GetChatMessageResponse()
        {
            ChatMessageModel = _modelMapper.Map(existingMessage)
        };
    }
}