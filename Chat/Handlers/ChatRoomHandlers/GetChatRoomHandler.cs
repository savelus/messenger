﻿using Chat.Data;
using Chat.Data.Entities;
using Chat.Mappers;
using Chat.Requests;
using Chat.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Chat.Handlers.ChatRoomHandlers;

public class GetChatRoomHandler : IRequestHandler<GetChatRoomRequest, GetChatRoomResponse>{
    
    private readonly ChatContext _ctx;
    private readonly ChatRoomModelMapper _modelMapper;

    public GetChatRoomHandler(ChatContext ctx, ChatRoomModelMapper modelMapper)
    {
        _ctx = ctx;
        _modelMapper = modelMapper;
    }

    public async Task<GetChatRoomResponse> Handle(GetChatRoomRequest roomRequest, CancellationToken cancellationToken)
    {
        var existingChats = await _ctx.ChatRooms
            .FirstOrDefaultAsync(x => x.Id == roomRequest.Id, cancellationToken);
        if (existingChats is not null)
        {
            //if(existingChats.Users.Contains())
            return new GetChatRoomResponse()
            {
                ChatRoomModel = _modelMapper.Map(existingChats)
            };
        }

        ChatRoom newChatRoom = new()
        {
            Title = roomRequest.Title
        };
        _ctx.ChatRooms.Add(newChatRoom);
        await _ctx.SaveChangesAsync(cancellationToken);
        return new GetChatRoomResponse()
        {
            ChatRoomModel = _modelMapper.Map(newChatRoom)
        };
    }
}