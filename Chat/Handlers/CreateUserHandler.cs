﻿using Chat.Data;
using Chat.Data.Entities;
using Chat.Mappers;
using Chat.Requests;
using Chat.Responses;
using Domain.Enum;
using MediatR;

namespace Chat.Handlers;

public class CreateUserHandler : IRequestHandler<CreateUserRequest, CreateUserResponse>
{
    private readonly ChatContext _ctx;
    private readonly ChatUserModelMapper _mapper;

    public CreateUserHandler(
        ChatContext ctx, 
        ChatUserModelMapper mapper)
    {
        _ctx = ctx;
        _mapper = mapper;
    }

    public async Task<CreateUserResponse> Handle(CreateUserRequest request, CancellationToken cancellationToken)
    {
        ChatUser newUser = new()
        {
            Name = request.User.Name,
            Login = request.User.Login,
            Password = request.User.Password,
            Role = (int)request.User.Role,
        };
        _ctx.ChatUsers.Add(newUser);
        await _ctx.SaveChangesAsync(cancellationToken);
        return new CreateUserResponse
        {
            StatusCode = StatusCode.OK
        };
    }
}