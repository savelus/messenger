﻿using Chat.Data;
using Chat.Data.Entities;
using Chat.Mappers;
using Chat.Requests;
using Chat.Responses;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Chat.Handlers;

public class GetChatUserHandler : IRequestHandler<GetChatUserRequest, GetChatUserResponse>
{
    private readonly ChatContext _ctx;
    private readonly ChatUserModelMapper _mapper;

    public GetChatUserHandler(
        ChatContext ctx, 
        ChatUserModelMapper mapper)
    {
        _ctx = ctx;
        _mapper = mapper;
    }

    public async Task<GetChatUserResponse> Handle(GetChatUserRequest request, CancellationToken cancellationToken)
    {
        var existingUser = await _ctx.ChatUsers
            .FirstOrDefaultAsync(x => x.Login == request.Login, cancellationToken);
        if (existingUser is not null)
        {
            return new GetChatUserResponse()
            {
                User = _mapper.Map(existingUser)
            };
        }
        ChatUser newUser = new()
        {
            Name = request.Name,
            Login = request.Login
        };
        _ctx.ChatUsers.Add(newUser);
        await _ctx.SaveChangesAsync(cancellationToken);
        return new GetChatUserResponse()
        {
            User = _mapper.Map(newUser)
        };
    }
}