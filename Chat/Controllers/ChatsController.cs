using Chat.Data;
using Chat.Requests;
using Chat.Requests.ChatMessageRequests;
using Chat.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Chat.Controllers;

[Route("/api/[controller]")]
[ApiController]
[Produces(System.Net.Mime.MediaTypeNames.Application.Json)]
public class ChatsController : Controller
{
    private readonly IMediator _mediator;

    public ChatsController(
        IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet("user")]
    public Task<GetChatUserResponse> GetChatUser(
        [FromQuery] GetChatUserRequest request)
        => _mediator.Send(request);

    [HttpGet("chat/{chatId:int}")]
    public Task<GetChatRoomResponse> GetChatUser([FromQuery] GetChatRoomRequest request, int chatId)
    {
        request.Id = chatId;
        return _mediator.Send(request);
    }

    [HttpGet("chat/{chatId:int}/{messageId:int}")]
    public Task<GetChatMessageResponse> GetChatMessage([FromQuery] GetChatMessageRequest request, int chatId, int messageId)
    {
        request.Id = messageId;
        request.ChatId = chatId;
        return _mediator.Send(request);
    }
    
    [HttpPost("chat/{chatId:int}/{messageId:int}")]
    public Task<GetChatMessageResponse> PostChatMessage([FromBody] PostChatMessageRequest request, int chatId, int messageId)
    {
        request.Id = messageId;
        request.ChatId = chatId;
        return _mediator.Send(request);
    }
    
    [HttpPut("chat/{chatId:int}/{messageId:int}")]
    public Task<GetChatMessageResponse> PutChatMessage([FromBody] PutChatMessageRequest request, int chatId, int messageId)
    {
        request.MessageId = messageId;
        request.ChatId = chatId;
        return _mediator.Send(request);
    }

}