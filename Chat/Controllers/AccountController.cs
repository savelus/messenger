﻿using System.Security.Claims;
using Chat.Domain.ViewModels.Account;
using Chat.Helpers;
using Chat.Models;
using Chat.Requests;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;

namespace Chat.Controllers;

[Route("/api/[controller]")]
[ApiController]
[Produces(System.Net.Mime.MediaTypeNames.Application.Json)]
public class AccountController : Controller
{
    private readonly IMediator _mediator;

    public AccountController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet("register")]
    public IActionResult Register() => View();

    [HttpPost("register")]
    public async Task<IActionResult> Register(RegisterViewModel model)
    {
        if (ModelState.IsValid)
        {
            var requestGet = new GetChatUserLoginRequest { Login = model.Login, };
            var responseGetUser = await _mediator.Send(requestGet);
            if (responseGetUser.User != null)
            {
                ModelState.AddModelError("", "Пользователь с таким логином уже существует");
            }
            else
            {
                var user = new ChatUserModel()
                {
                    Name = model.Name,
                    Login = model.Login,
                    Role = Role.User,
                    Password = HashPasswordHelper.HashPassword(model.Password),
                };
                var requestCreate = new CreateUserRequest { User = user, };
                var responseCreate = _mediator.Send(requestCreate);

                if (responseCreate.Result.StatusCode == global::Domain.Enum.StatusCode.OK)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.ToString())
                    };
                    var сlaimsIdentity = new ClaimsIdentity(claims, "ApplicationCookie",
                        ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                        new ClaimsPrincipal(сlaimsIdentity));
                    return RedirectToAction("", "Chats"); //TODO переадресация после аутентификации
                }

                ModelState.AddModelError("", "Ошибка регистрации пользователя");
            }
        }

        return View(model);
    }

    [HttpGet("login")]
    public IActionResult Login() => View();

    [HttpPost("login")]
    public async Task<IActionResult> Login(LoginViewModel model)
    {
        if (ModelState.IsValid)
        {
            var requestGetUser = new GetChatUserLoginRequest { Login = model.Login, };
            var responseGetUser = await _mediator.Send(requestGetUser);
            var user = responseGetUser.User;
            if (user == null)
            {
                ModelState.AddModelError("", "Пользователь не найден");
            }
            else if (HashPasswordHelper.HashPassword(model.Password) != user.Password)
            {
                ModelState.AddModelError("", "Введен неверный пароль");
            }
            else
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role.ToString())
                };
                var сlaimsIdentity = new ClaimsIdentity(claims, "ApplicationCookie",
                    ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(сlaimsIdentity));
                return RedirectToAction("", "Chats"); //TODO переадресация после аутентификации
            }
        }

        return View(model);
    }

    [HttpGet("logout")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Logout()
    {
        await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        return RedirectToAction("", "Chats"); //TODO переадресация после аутентификации
    }
}