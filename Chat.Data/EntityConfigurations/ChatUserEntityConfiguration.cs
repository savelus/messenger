﻿using Chat.Data.Entities;
using Chat.Data.Entities.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Chat.Data.EntityConfigurations;

public class ChatUserEntityConfiguration : IEntityTypeConfiguration<ChatUser>
{
    public void Configure(EntityTypeBuilder<ChatUser> builder)
    {
        builder.HasKey(x => x.Id);
        builder.HasMany(x=>x.Messages)
            .WithOne(x=>x.Author)
            .HasForeignKey(x => x.ChatRoomId);

        builder.HasMany(x => x.Chatrooms)
            .WithMany(x => x.Users);

        builder.SoftDeletable();
    }
}