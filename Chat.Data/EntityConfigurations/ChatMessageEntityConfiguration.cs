﻿using Chat.Data.Entities;
using Chat.Data.Entities.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Chat.Data.EntityConfigurations;

public class ChatMessageEntityConfiguration : IEntityTypeConfiguration<ChatMessage>
{
    public void Configure(EntityTypeBuilder<ChatMessage> builder)
    {
        builder.HasKey(x => x.Id);
        builder.HasOne(x => x.Author)
            .WithMany(x => x.Messages)
            .HasForeignKey(x => x.AuthorId);
        
        builder.HasOne(x => x.ChatRoom)
            .WithMany(x => x.Messages)
            .HasForeignKey(x => x.ChatRoomId);

        builder.SoftDeletable();
    }
}