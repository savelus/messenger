﻿using Chat.Data.Entities;
using Chat.Data.Entities.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Chat.Data.EntityConfigurations;

public class ChatRoomEntityConfiguration : IEntityTypeConfiguration<ChatRoom>
{
    public void Configure(EntityTypeBuilder<ChatRoom> builder)
    {
        builder.HasKey(x => x.Id);
        builder.HasMany(x=>x.Messages)
            .WithOne(x=>x.ChatRoom)
            .HasForeignKey(x => x.ChatRoomId);

        builder.HasMany(x => x.Users)
            .WithMany(x => x.Chatrooms)
            .UsingEntity(x => x.ToTable("UsersInChats"));

        builder.SoftDeletable();
    }
}