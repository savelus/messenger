using Chat.Data.Entities.Abstractions;

namespace Chat.Data.Entities;

public class ChatUser : ISoftDeletable
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Login { get; set; }
    public string Password { get; set; }
    public int Role { get; set; }
    public ICollection<ChatRoom> Chatrooms { get; set; } = new List<ChatRoom>();
    public ICollection<ChatMessage> Messages { get; set; } = new List<ChatMessage>();

    public bool IsDeleted { get; set; }
    public DateTime? DeletedTime { get; set; }
}