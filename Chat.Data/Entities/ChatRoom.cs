using Chat.Data.Entities.Abstractions;

namespace Chat.Data.Entities;

public class ChatRoom : ISoftDeletable
{
    public int Id { get; set; }
    public string Title { get; set; }
    public ICollection<ChatUser> Users { get; set; } = new List<ChatUser>();
    public ICollection<ChatMessage> Messages { get; set; } = new List<ChatMessage>();
    
    public bool IsDeleted { get; set; }
    public DateTime? DeletedTime { get; set; }
}