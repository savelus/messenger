﻿namespace Chat.Data.Entities.Abstractions;

public interface ISoftDeletable
{
    public bool IsDeleted { get; set; }
    public DateTime? DeletedTime { get; set; }
}