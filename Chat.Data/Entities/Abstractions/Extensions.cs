﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Chat.Data.Entities.Abstractions;

public static class Extensions
{
    public static EntityTypeBuilder<TSoftDeletable> SoftDeletable<TSoftDeletable>(
        this EntityTypeBuilder<TSoftDeletable> builder)
        where TSoftDeletable : class, ISoftDeletable
    {
        builder.Property(x => x.IsDeleted)
            .HasDefaultValue(false);
        builder.Property(x => x.DeletedTime)
            .HasDefaultValue(null)
            .IsRequired(false);
        builder.HasQueryFilter(x => x.IsDeleted == false);
        return builder;
    }
}