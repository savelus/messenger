using Chat.Data.Entities.Abstractions;

namespace Chat.Data.Entities;

public class ChatMessage : ISoftDeletable
{
    public int Id { get; set; }
    public string Text { get; set; }
    
    public ChatUser Author { get; set; }
    public int AuthorId { get; set; }
    
    public ChatRoom ChatRoom { get; set; }
    public int ChatRoomId { get; set; }
    
    public bool IsDeleted { get; set; }
    public DateTime? DeletedTime { get; set; }
    
}