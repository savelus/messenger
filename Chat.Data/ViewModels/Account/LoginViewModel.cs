﻿using System.ComponentModel.DataAnnotations;

namespace Chat.Domain.ViewModels.Account;

public class LoginViewModel
{
    [Required(ErrorMessage = "Укажите логин")]
    public string Login { get; set; }
    
    [DataType(DataType.Password)]
    [Required(ErrorMessage = "Укажите пароль")]
    [Display(Name = "Пароль")]
    public string Password { get; set; }
}