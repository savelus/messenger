using Chat.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Chat.Data;

public class ChatContext : DbContext
{
    public DbSet<ChatRoom> ChatRooms { get; set; }
    public DbSet<ChatUser> ChatUsers { get; set; }
    public DbSet<ChatMessage> ChatMessages { get; set; }

    private static bool firstRun = true;
    public ChatContext(DbContextOptions<ChatContext> options)
    : base(options)
    {
        if (!firstRun) return;
        Database.Migrate();
        firstRun = false;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(ChatContext).Assembly);
    }
    
    
}